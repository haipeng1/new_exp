﻿namespace BLL
{
    public class CheckPeople
    {
        //核对身份证后4位是否正确

        public int Check(Model.SignUpPerson sp)
        {
            DAL.ID4 id4 = new DAL.ID4();
            string id = id4.GetID4(sp.stuid); //获取该学号对应的身份证后4位

            //return 0;

            if(sp.id == id) //身份证后4位正确
            {
                return 0;
            }
            else
            {
                return 2; //身份证后4位错误，返回 2
            }
        }
    }
}
