﻿using System;

namespace Model
{
    public class Page
    {
        public long id { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string publisher { get; set; }
        public DateTime set_time { get; set; }
        public DateTime edit_time { get; set; }
        public string main { get; set; }
        public int good { get; set; }
    }
}
