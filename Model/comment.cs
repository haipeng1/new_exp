﻿using System;

namespace Model
{
    public class Comment
    {
        public string username { get; set; }   //评论者
        public string text { get; set; }    //评论内容
        public long page_id { get; set; }   //文章id
        public long id { get; set; }       //评论表id
        public DateTime time { get; set; }  //发表时间
    }
}
