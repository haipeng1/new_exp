﻿using System.Text.RegularExpressions;

namespace WebUI.handler
{
    public static class Safe
    {
        public static string StringSafeHtml(string unsafe_html_string)
        {
            //把所有 < 都替换成 (<)
            string pattern = "\\<([A-Za-z0-9]{1,})";
            string replacement = "(<)$1";
            string safe_string = Regex.Replace(unsafe_html_string, pattern, replacement);

            //换回<p>标签
            pattern = "(<)p";
            replacement = "<p";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<p>标签
            pattern = "(<)h";
            replacement = "<h";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<img>标签
            pattern = "(<)img";
            replacement = "<img";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<span>标签
            pattern = "(<)span";
            replacement = "<span";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<em>标签
            pattern = "(<)em";
            replacement = "<em";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<u>标签
            pattern = "(<)u";
            replacement = "<u";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<strong>标签
            pattern = "(<)strong";
            replacement = "<strong";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<strike>标签
            pattern = "(<)strike";
            replacement = "<strike";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<sup>标签
            pattern = "(<)sup";
            replacement = "<sup";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<sub>标签
            pattern = "(<)sub";
            replacement = "<sub";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<ul>标签
            pattern = "(<)ul";
            replacement = "<ul";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<ol>标签
            pattern = "(<)ol";
            replacement = "<ol";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<li>标签
            pattern = "(<)li";
            replacement = "<li";
            safe_string = safe_string.Replace(pattern, replacement);

            //换回<a>标签
            pattern = "(<)a";
            replacement = "<a";
            safe_string = safe_string.Replace(pattern, replacement);

            return safe_string;
        }
    }
}