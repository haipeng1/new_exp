﻿using System;
using System.Web;
using System.IO;
using System.Web.SessionState;

namespace new_exp.handler
{
    /// <summary>
    /// uploadfile 的摘要说明
    /// </summary>
    public class uploadfile : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";

            //验证是否管理员
            //if (context.Session["admin"] == null)
            //{
            //    return;
            //}

            HttpPostedFile file = context.Request.Files["upload"]; //获取上传的文件
            String callback = context.Request["CKEditorFuncNum"];
            string errorMsg = null;
            string script = @"
            <script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction({0}, '{1}', '{2}');</script>";
            //对CKeditor的配置

            string filename = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName); //将文件名设置成guid值，避免重复

            string filepath = "/page_img/" + filename; //生成文件路径

            //filename = GetMD5HashFromFile(file);

            string manpath = context.Server.MapPath(filepath); //用context.Server.MapPath()生成绝对路径

            try
            {
                file.SaveAs(manpath); //保存文件，SaveAs()方法需要绝对路径
            }
            catch (Exception ex)
            {
                filename = null;
                errorMsg = ex.Message;
            }

            context.Response.Write(string.Format(script, callback, "/page_img/" + filename, errorMsg));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}