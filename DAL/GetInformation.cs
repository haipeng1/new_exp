﻿using Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{
    public class GetInformation   //宾悦：从数据库中获得所有的信息
    {
        //从数据库取新闻（包括 标题、时间、作者等等）
        public DataTable GetNews(string type, int Start, int End)    //宾悦：从数据库取新闻（包括 标题、时间、作者等等）
        {
            DataTable News = new DataTable();
            //News = SqlHelper.ExecuteDataTable("select * from  这里写装新闻（包括：标题、时间、作者等等）的表的表名 where type=@type", new SqlParameter("@type", type));
            News = SqlHelper.ExecuteDataTable("select * from ( select * row_number() over (order by Id asc) as num from 新闻表名 where type=@type ) where num between @Start and @End", new SqlParameter("@type", type), new SqlParameter("@Start", Start), new SqlParameter("@End", End));
            return News;
        }

        //首页的数据调用
        public DataTable IndexNews(string type,int number)
        {
            DataTable indexNews = new DataTable();
            indexNews = SqlHelper.ExecuteDataTable("select top @number * from 新闻表名 where type=@type order by edit_time desc ", new SqlParameter("@type", type), new SqlParameter("@number", number));
            return indexNews;
        }

        //查询指定类型的新闻的总条数
        public int NewsCount(string tablename,string type) //参数：第一个是一个数据表名，第二个是新闻类型。第一个参数可以考虑不需要
        {
            int count = (int)SqlHelper.ExecuteScalar("select count(*) from @tablename where type=@type", new SqlParameter("@tablename", tablename), new SqlParameter("@type", type));
            return count;
        }

        //查询新闻正文
        public DataTable GetArticle(string ArticleId)
        {
            DataTable article = new DataTable();
            article = SqlHelper.ExecuteDataTable("select * from 新闻表名 where id=@ArticleId", new SqlParameter("@ArticleId", ArticleId));
            return article;
        }

        //获得所有制定类型的新闻
        public DataTable GetAllArticle(string type)
        {
            DataTable article = new DataTable();
            article = SqlHelper.ExecuteDataTable("select * from 新闻表名 where type=@type", new SqlParameter("@type", type));
            return article;
        }
    }
}
